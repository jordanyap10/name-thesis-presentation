\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Background}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Problem Statement}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Research Objectives}{12}{0}{1}
\beamer@subsectionintoc {1}{4}{Research Scope}{13}{0}{1}
\beamer@sectionintoc {2}{Research Methodology}{14}{0}{2}
\beamer@sectionintoc {3}{Results and Discussion}{16}{0}{3}
\beamer@sectionintoc {4}{Conclusions and Recommendations}{18}{0}{4}

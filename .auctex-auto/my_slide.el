(TeX-add-style-hook
 "my_slide"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ccicons" "scale=2") ("babel" "english")))
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "appendixnumberbeamer"
    "booktabs"
    "ccicons"
    "pgfplots"
    "xspace"
    "amsmath"
    "amssymb"
    "diffcoeff"
    "babel"
    "blindtext"
    "caption"
    "siunitx")
   (TeX-add-symbols
    "themename"
    "saveenum"
    "resume")
   (LaTeX-add-labels
    "fig:a"
    "fig:b"
    "fig:c"
    "fig:d"
    "Total_Weight_Details"
    "Airfoil_Characteristic"
    "Major_Step"
    "mesh"
    "Airfield_phase"
    "Force_acting_on_UAV"
    "Force_acting_on_UAV_during_landing"
    "ISA_Graph"
    "pressure"
    "Wing_Characteristic"
    "Wing_Characteristic(2)"
    "Performance_Variables"
    "Take_Off_Performance"
    "Airborne_Performance"
    "Landing_distance_performance_due_to_Fuel_Remaining"
    "Gliding_Performance"
    "Climbing_Performance_due_to_Altitude"
    "Climbing_Performance_due_to_Altitude_Continued"
    "Climbing_Performance_with_Fuel_Reduction_due_to_Altitude"
    "Climbing_Performance_with_Fuel_Reduction_due_to_Altitude_Continued"
    "Vh_vs_Rate_of_Decent")
   (LaTeX-add-counters
    "savedenum"))
 :latex)

